import React, { Component } from "react";

class FaceGrid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userImgs: []
    };
  }

  componentWillMount() {
    const userAPI = "https://randomuser.me/api/?results=256";
    fetch(userAPI)
      .then(response => {
        return response.json();
      })
      .then(data => {
        const users = data.results;
        const userImgs = users.map(user => {
          return (
            <div key={user.email}>
              <img className="userimg" alt="" src={user.picture.large} />
            </div>
          );
        });
        this.setState({ userImgs: userImgs });
      });
  }

  render() {
    return (
      <div>
        <div>{this.state.userImgs}</div>
      </div>
    );
  }
}

export default FaceGrid;
