import React, { Component } from "react";
import FaceGrid from "./FaceGrid.js";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <FaceGrid />
      </div>
    );
  }
}

export default App;
